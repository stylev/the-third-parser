import requests
from bs4 import BeautifulSoup as Bs
import csv


def get_html(url):
    """
    get html code of the page
    :param url:
    :return: html code
    """
    r = requests.get(url)
    return r.text


def write_csv(data):
    """

    :param data:
    :return:
    """
    with open('cmc.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow([
            data['name'],
            data['symbol'],
            data['url'],
            data['price']
        ])


def get_data(url):
    """

    :param url:
    :return:
    """
    soup = Bs(get_html(url), 'lxml')
    trs = soup.find('table', {'id': 'currencies'}).find('tbody').find_all('tr')
    for tr in trs:
        tds = tr.find_all('td')
        name = tds[1].find('a', {'class': 'currency-name-container'}).text
        symbol = tds[1].find('a').text
        url = 'https://coinmarketcap.com' + tds[1].find('a').get('href')
        price = tds[3].find('a').get('data-usd')
        data = {
            'name': name,
            'symbol': symbol,
            'url': url,
            'price': price
        }
        write_csv(data)


def main():
    url = 'https://coinmarketcap.com/'
    get_data(url)


if __name__ == '__main__':
    main()
